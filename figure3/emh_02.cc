//------------------------------------------------------------------------------
// Compute Temperature Distribution In Heavy Oil Reservoirs By Electromagnetic
// Heating. This code using staggered finite difference time domain for Maxwell
// equations and  basic finite differences for the heating diffusion equation
// in the reservoir.
// gherling@gmail.com (2021)
// herling.gonzalez@ecopetrol.com.co (2021)
//------------------------------------------------------------------------------
#include <valarray>
#include <iostream>
#include <cstdio>
#include <cmath>

// Electromagnetic constants
namespace Const {
  const double C = 299792458.0;        // [m/s] speed of light
  const double EPS_0 = 8.85418782e-12; // [C/V/m] vacuum electric permittivity
  const double MU_0  = 1./(C*C*EPS_0); // [H/m] vacuum magnetic permeability
};

using std::cout;
using std::endl;
using Array = std::valarray<float>;

// ouput data fields cvs format
void outputFields(char* name, //file csv name
                  float x0,   //origin data
                  float dh,   //grid size
                  const Array &eps,   //relative electric permittivity
                  const Array &sig,   //electric conductivity
                  const Array &Ex,    //electric field
                  const Array &Ener)    //Energy density field
{
	FILE *outfile = fopen(name,"w");
	fprintf(outfile,"x,eps_r,sigma,Ex,Energy\n"); //header
	for (size_t i=0; i<Ex.size(); i++) {
		fprintf(outfile,"%g,%g,%g,%g,%g\n",x0+(i*dh),eps[i],sig[i],Ex[i],Ener[i]);
	}
	fclose(outfile);
}

//absorbing boundary conditions
void abc(float &l1, //left boundary step n-1
         float &l2, //left boundary step n
         float &r1, //righ boundary step n-1
         float &r2, //righ boundary step n
         Array &Ex) //electric field
{
	int n = Ex.size();
	//left boundary
	Ex[0] = l2;
	l2    = l1;
	l1    = Ex[1];
	//righ boundary
	Ex[n-1] = r2;
	r2    = r1;
	r1    = Ex[n-2];
}

//initial dielectric properties reservoir
void initMaterial(float dh, float L, Array &Er, Array &Sigm){
  int n1,n2,n3,n4;
  float m1,m2,m3,m4;
  m1 = 5.0;  //clay zone  [m]
  m2 = 3.0;  //water zone [m]
  m3 = 13.0; //recervoir  [m]
  m4 = 5.0;  //clay zone  [m]
  n1 = int(m1/dh)+1;
  n2 = int(m2/dh);
  n3 = int(m3/dh);
  n4 = int(m4/dh)+1;
  for (int i=0; i<n1; i++){ //Clay zone
      Er[i] = 21.6;
      Sigm[i] = 0.0192;//[S/m]
  }
  for (int i=n1; i<n1+n2; i++){ //water zone
      Er[i] = 70.84;
      Sigm[i] = 0.0519;//[S/m]
  }
  for (int i=n1+n2; i<n1+n2+n3; i++){ //reservoir zone
      Er[i] = 5.6;
      Sigm[i] = 0.0014;//[S/m]
  }
  for (int i=n1+n2+n3; i<n1+n2+n3+n4; i++){ //Clay zone
      Er[i] = 21.6;
      Sigm[i] = 0.0192;//[S/m]
  }    
}

int main(int argc,char* argv[]) {

	int N,T;
	float E0,Zc,potency;
	float dt,dh,fq,sc;
	float L; //dimension of model [m]

	//modeling parameters
	potency = 5e3;    //antenna source power [Watt]
	Zc = 1.0; //81.6;  //reservoir impendance [Ohm]
	E0 = sqrt(2*potency*Zc); //electrical amplitude pulse [V]
	fq = 400e6;         //electromagnetic frequency of antenna [Hz]
  sc = 100;           //Cycle-period subdivision for the frequency
  dt = 1.0/(sc*fq);   //fraction of the oscillation period
	dh = dt*2*Const::C; //grid size from stability condition
	L  = 26;            // dimension of model [m]
	N  = int(L/dh)+1;   // number of cells
	T  = 12500;          // number of steps time

	// array fields definition
	Array Ex(N),Hy(N); //electromagnetic field
	Array sigma(N),EPS_r(N);//dielectric reservoir properties

  initMaterial(dh,L,EPS_r,sigma); //initial properties of Media  

 	cout << "***********" << endl;
	cout << "dt[ns] = " << dt*1e9 << endl;
	cout << "dh[cm] = " << dh*100 << endl;
	cout << "L[m] = " << L << endl;
	cout << "Tout = " << T << endl;
	cout << "Cells = " << N << endl;
  cout << "Cycle subdivision = " << sc << endl; 
	cout << "Time[micro-seg] = " << T*dt*1e6 << endl;
	cout << "Frq[MHz] = " << fq*1e-6 << endl;
	cout << "Potency[kW] = " << potency*1e-3 << endl;
	cout << "***********" << endl;

	float eaf,ca,cb;  //auxiliary variables

	//static: It's global variable scope
	//during function calling "void abc()"
	static float l1,l2,r1,r2;

	//EM step-time loop
	for (int it=0; it<T; it++) {

		//update electric field
		for (int k=1; k<N; k++) {
			eaf = dt*sigma[k]/(2*EPS_r[k]*Const::EPS_0);
			ca = (1-eaf)/(1+eaf);
			cb = 0.5/(EPS_r[k]*(1+eaf));
			Ex[k] = ca*Ex[k] + cb*( Hy[k-1]-Hy[k] );
		}
		//add pulse source in middle position
		Ex[N/2] += E0*sin(2*M_PI*fq*it*dt);
		
		//absorbin boundary condition
		abc(l1,l2,r1,r2,Ex);

		//update magnetic field
		for (int k=0; k<N-1; k++) {
			Hy[k] += 0.5*( Ex[k]-Ex[k+1] );
		}

	}//EM end step-time loop

	
	float Vol = 1*1*26; //Simulated reservoir volume
	Array Energy(N);    //Energy density Simulation

	// Electric field energy density 
  for (int k=0; k<N; k++) {
		Energy[k] = Ex[k]*Ex[k]*0.5*sigma[k]/Vol; //[J/m^3]
	}	

	//ouput fields to CSV file
	outputFields((char*)"result.csv",0,dh,EPS_r,sigma*1e3,Ex,Energy*20);

}
