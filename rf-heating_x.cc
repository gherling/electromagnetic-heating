//------------------------------------------------------------------------------
// Methodological approach to estimate temperature variation in heavy crude oils 
// by electromagnetic absorption. herling.gonzalez@ecopetrol.com.co (2021)
//------------------------------------------------------------------------------
#include <valarray>
#include <iostream>
#include <cstdio>
#include <cmath>

using std::cout;
using std::endl;
using Array = std::valarray<float>;

// constants 
namespace Const {
  const double C = 299792458.0;        // [m/s] speed of light 
  const double EPS_0 = 8.85418782e-12; // [C/V/m] vacuum electric permittivity
  const double MU_0  = 1./(C*C*EPS_0); // [H/m] vacuum magnetic permeability
};

// Thermal conductivity function [W/m/ºK]
inline float kappa(float T) { 
  return 1.6298 - (T)*(0.0003 + (T)*(1e-6 - 7e-10*(T))); 
}

// Initial reservoir properties of Media
void initMaterial(float dh,    //grid size [m]
                  float L,     //dimension of model [m]
                  Array &Er,   //relative electric permittivity
                  Array &Sigm, //electric conductivity [mS/m]
                  Array &rho,  //density [kg/m^3]
                  Array &cp);  //caloric capacity

// Export electric field data 
void outputFields(char* name,          //file csv name 
                  float x0, float dh,  //origin data and interval
                  const Array &eps,    //relative electric permittivity
                  const Array &sig,    //electric conductivity
                  const Array &Ex);    //electric field

// Export temperature reservoir distribution 
void outputTemp(char *name,      //file csv name
                float x0,        //origin data
                float dh,        //interval data
                const Array &T); //temperature

// Absorbin Boundary Condition
void abc(float &l1,float &l2,float &r1,float &r2,Array &Ex);

//------------------------------------------------------------------------------
//345678901234567890123456789012345678901234567890123456789012345678901234567890
//-------10--------20--------30--------40--------50--------60--------70-------80
int main(int argc,char* argv[]) {

  int N,T;
  float E0,Zc,potency;
  float dt,dh,fq,sc;
  float L; //dimension of model [m]
  //parameters
  potency = 20e3; //antenna source power [Watt]
  Zc = 1.0; //81.6;        //antenna electric impendance [Ohm] 
  E0 = sqrt(2*potency*Zc); //electrical amplitude pulse [V]
  fq = 400e6;         //electromagnetic frequency of antenna [Hz]
  sc = 100;            //Cycle-period subdivision for the frequency
  dt = 1.0/(sc*fq);   //fraction of the oscillation period
  dh = dt*2*Const::C; //grid size from stability condition
  L  = 26; // dimension of model [m]
  N  = int(L/dh)+1; // number of cells
  T  = 12000; // steps time
  
  Array Ex(N),Hy(N); //electromagnetic field
  Array sigma(N),EPS_r(N); //material properties
  Array rho(N),cp(N);
  
  initMaterial(dh,L,EPS_r,sigma,rho,cp);//initial properties of Media

  cout << "***********" << endl;
  cout << "dt[ns] = " << dt*1e9 << endl; 
  cout << "dh[cm] = " << dh*100 << endl; 
  cout << "Cells = " << N << endl; 
  cout << "Cycle subdivision = " << sc << endl; 
  cout << "L[m] = " << L << endl; 
  cout << "Time[micro-seg] = " << T*dt*1e6 << endl; 
  cout << "Frq[MHz] = " << fq*1e-6 << endl; 
  cout << "Potency[kW] = " << potency*1e-3 << endl; 
  cout << "***********" << endl;
 
  //static: It's global access memory because during 
  //function calling "void abc()" make changes to variables
  float eaf,ca,cb;  //auxiliary variables
  static float l1,l2,r1,r2;//auxiliary abc variables 


  //EM numerical solution step-time loop
  for (int it=0; it<T; it++) {
    
    //update electric field
    for (int k=1; k<N; k++) {
      eaf = dt*sigma[k]/(2*EPS_r[k]*Const::EPS_0);
      ca = (1-eaf)/(1+eaf);
      cb = 0.5/(EPS_r[k]*(1+eaf)); 
      Ex[k] = ca*Ex[k] + cb*( Hy[k-1]-Hy[k] );
    }
    
    //add source pulse
    Ex[N/2] += E0*sin(2*M_PI*fq*it*dt); 
    
    //absorbin boundary condition
    abc(l1,l2,r1,r2,Ex);

    //update magnetic field
    for (int k=0; k<N-1; k++) {
      Hy[k] += 0.5*( Ex[k]-Ex[k+1] );
    }

  }//EM end step-time loop

  //ouput electric field to CSV file 
  outputFields((char*)"em-field.csv",0,dh,EPS_r,sigma,Ex);

  float gm,kp;          //termal reservoir properties
  float Vol = 1*1*26;   //simulated reservoir volume
  float Dt = 60.0;      //termic time-step [seconds]
  int  Days = 24*60*Dt; //time-steps by day
  int  Tout = 60*Days;  //total time-steps
  char namefile[20];    //file name to export

  Array T1(N); //Temperature distribution in reservoir

  //initial reservoir temperature  
  for (int k=0; k<N; k++) {
    T1[k] = 373.0;
  }

  // Electric field energy source 
  for (int k=0; k<N; k++) {
    Ex[k] = Ex[k]*Ex[k]*0.5*sigma[k]/Vol;
  }	

  //termic equation numerical solution (step-time loop)
  for (int it=1; it<Tout+1; it++) {
    for (int k=1; k<N-1; k++) {
      gm = rho[k]*cp[k];
      kp = kappa(T1[k]);
      T1[k] = T1[k] + Dt*( (T1[k+1]-2*T1[k]+T1[k-1])*kp/(dh*dh*gm) + Ex[k]/gm );
    }
    // Export temperature distribution every n days
    if (it%(5*Days)==0){ 
      sprintf(namefile,"%d.csv",it/Days);	
      outputTemp(namefile,0,dh,T1-273.15);
    }
  }//end step-time loop

  return(0);
}
//------------------------------------------------------------------------------
//345678901234567890123456789012345678901234567890123456789012345678901234567890
//-------10--------20--------30--------40--------50--------60--------70-------80
void initMaterial(float dh,float L,Array &Er,Array &Sigm,Array &rho,Array &cp){
  int n1,n2,n3,n4;
  float m1,m2,m3,m4;
  m1 = 5.0;  //clay zone  [m]
  m2 = 3.0;  //water zone [m]
  m3 = 13.0; //recervoir  [m]
  m4 = 5.0;  //clay zone  [m]
  n1 = int(m1/dh)+1;
  n2 = int(m2/dh);
  n3 = int(m3/dh);
  n4 = int(m4/dh)+1;
  for (int i=0; i<n1; i++){ //Clay zone
      Er[i] = 21.6;
      Sigm[i] = 0.0192;//[S/m]
      rho[i] = 1700;   //[kg/m^3]
      cp[i] = 837.36;  //[J/kgK]
  }
  for (int i=n1; i<n1+n2; i++){ //water zone
      Er[i] = 70.84;
      Sigm[i] = 0.0519;//[S/m]
      rho[i] = 1010;   //[kg/m^3]
      cp[i] = 4186.8;  //[J/kgK]
  }
  for (int i=n1+n2; i<n1+n2+n3; i++){ //reservoir zone
      Er[i] = 5.6;
      Sigm[i] = 0.0014;//[S/m]
      rho[i] = 1300;   //[kg/m^3]
      cp[i] = 1465.38; //[J/kgK]
  }
  for (int i=n1+n2+n3; i<n1+n2+n3+n4; i++){ //Clay zone
      Er[i] = 21.6;
      Sigm[i] = 0.0192;//[S/m]
      rho[i] = 1700;   //[kg/m^3]
      cp[i] = 837.36;  //[J/kgK]
  }    
}



//------------------------------------------------------------------------------
//345678901234567890123456789012345678901234567890123456789012345678901234567890
//-------10--------20--------30--------40--------50--------60--------70-------80
void outputFields(char* name, float x0, float dh, 
                              const Array &eps, 
                              const Array &sig, 
                              const Array &Ex)
{
  FILE *outfile = fopen(name,"w");
  fprintf(outfile,"x,eps_r,sigma,Ex\n"); //header
  for (size_t i=0; i<Ex.size(); i++) {
    fprintf(outfile,"%g,%g,%g,%g\n",x0+(i*dh),eps[i],1e3*sig[i],Ex[i]);
  }
  fclose(outfile);
}

//------------------------------------------------------------------------------
//345678901234567890123456789012345678901234567890123456789012345678901234567890
//-------10--------20--------30--------40--------50--------60--------70-------80
void outputTemp(char *name, float x0, float dh, const Array &T){
  FILE *outfile = fopen(name,"w");
  fprintf(outfile,"Z[m],Temp[ºC]\n"); //header
  for (size_t i=0; i<T.size(); i++) {
    fprintf(outfile,"%g,%g\n",x0+(i*dh),T[i]);
  }
  fclose(outfile);
}

//------------------------------------------------------------------------------
//345678901234567890123456789012345678901234567890123456789012345678901234567890
//-------10--------20--------30--------40--------50--------60--------70-------80
void abc(float &l1,float &l2,float &r1,float &r2,Array &Ex) {
  int n = Ex.size();
  //left boundary
  Ex[0] = l2;
  l2    = l1;
  l1    = Ex[1];
  //righ boundary
  Ex[n-1] = r2;
  r2    = r1;
  r1    = Ex[n-2];
}
