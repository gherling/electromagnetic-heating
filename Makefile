# Makefile for compiling files proyect 
# in c++ by Herling Gonzalez-Alvarez
# gherling@gmail.com

EXEC  = EMH
FILES = rf-heating_x

SRCS = $(FILES:=.cc)
OBJS = $(SRCS:.cc=.o)

CC = g++ 
CFLAGS = -O3 -Wall -fopenmp 

INCS = -I/usr/include/
LIBS = -L/usr/lib -lm -fopenmp 

$(EXEC): $(OBJS)
	echo compiling $@...
	$(CC) $(CFLAGS)   $(INCS) $(OBJS) -o $(EXEC) $(LIBS) 

.cc.o : 
	$(CC) $(INCS) $(CFLAGS) -I. -c $< -o $@

run:
	./$(EXEC)

clean:
	-rm $(EXEC) *.o *~ *.csv 
