# Electromagnetic Heating

 We present a numerical scheme to estimate temperature variations in the reservoir by electromagnetic absorption, based on the calculation of the amplitude of the electromagnetic wave field and the radiation heat diffusion coupling. The electrical and thermal properties of the reservoir were calculated taking into account the fractions and saturation of its phases and the results obtained from this RF heating simulation show a radially distributed temperature profile within the reservoir, where the power and frequency of the incident wave were considered for an antenna situated in the middle of the formation. This enables us to determine the energy required in kWh and its influence on the antenna's power as well as the thermal and electrical properties of the medium to reach a steady temperature inside the reservoir in days or months.


## License

This work is licensed under the GNU General Public License (GPL) v3.0. See the
`LICENSE` file for more details.

## Authors

You can contact the author using the following email adresses:
* Herling Gonzalez Alvarez: <gherling@gmail.com>
