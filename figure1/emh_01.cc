//------------------------------------------------------------------------------
// Compute Temperature Distribution In Heavy Oil Reservoirs By Electromagnetic
// Heating. This code using staggered finite difference time domain for Maxwell
// equations and  basic finite differences for the heating diffusion equation
// in the reservoir.
// gherling@gmail.com (2021)
// herling.gonzalez@ecopetrol.com.co (2021)
//------------------------------------------------------------------------------
#include <valarray>
#include <iostream>
#include <cstdio>
#include <cmath>

// Electromagnetic constants
namespace Const {
  const double C = 299792458.0;        // [m/s] speed of light
  const double EPS_0 = 8.85418782e-12; // [C/V/m] vacuum electric permittivity
  const double MU_0  = 1./(C*C*EPS_0); // [H/m] vacuum magnetic permeability
};

using std::cout;
using std::endl;
using Array = std::valarray<float>;

// ouput data fields cvs format
void outputFields(char* name, //file csv name
                  float x0,   //origin data
                  float dh,   //grid size
                  const Array &eps,   //relative electric permittivity
                  const Array &sig,   //electric conductivity
                  const Array &Ex)    //electric field
{
	FILE *outfile = fopen(name,"w");
	fprintf(outfile,"x,eps_r,sigma,Ex\n"); //header
	for (size_t i=0; i<Ex.size(); i++) {
		fprintf(outfile,"%g,%g,%g,%g\n",x0+(i*dh),eps[i],sig[i],Ex[i]);
	}
	fclose(outfile);
}

//absorbing boundary conditions
void abc(float &l1, //left boundary step n-1
         float &l2, //left boundary step n
         float &r1, //righ boundary step n-1
         float &r2, //righ boundary step n
         Array &Ex) //electric field
{
	int n = Ex.size();
	//left boundary
	Ex[0] = l2;
	l2    = l1;
	l1    = Ex[1];
	//righ boundary
	Ex[n-1] = r2;
	r2    = r1;
	r1    = Ex[n-2];
}

int main(int argc,char* argv[]) {

	int N,T;
	float E0,Zc,potency;
	float dt,dh,fq;
	float L; //dimension of model [m]

	//modeling parameters
	potency = 1e3;    //antenna source power [Watt]
	Zc = 1.0; //81.6;  //reservoir impendance [Ohm]
	E0 = sqrt(2*potency*Zc); //electrical amplitude pulse [V]
	fq = 150e6;         //electromagnetic frequency of antenna [Hz]
  dt = 1.0/(70*fq);   //fraction of the oscillation period
	dh = dt*2*Const::C; //grid size from stability condition
	L  = 20; // dimension of model [m]
	N  = int(L/dh)+1; // number of cells
	T  = 2500; // number of steps time

	// array fields definition
	Array Ex(N),Hy(N); //electromagnetic field
	Array sigma(N),EPS_r(N);//dielectric reservoir properties

	//sandstone dry reservoir media
	for (int i=0; i<N/2; i++){
		EPS_r[i] = 5;       //relative permittivity
		sigma[i] = 0.00001; //conductivity [S/m]
	}

/********
	//oil-sand reservoir media
	for (int i=N/2; i<N; i++){
		EPS_r[i] = 5.6;    //relative permittivity
		sigma[i] = 0.0014; //conductivity [S/m]
	}
*********/

	//Clay/Shale reservoir media
	for (int i=N/2; i<N; i++){ 
		EPS_r[i] = 21.6;   //relative permittivity
		sigma[i] = 0.0192; //conductivity [S/m]
	}

 	cout << "***********" << endl;
	cout << "dt[ns] = " << dt*1e9 << endl;
	cout << "dh[cm] = " << dh*100 << endl;
	cout << "L[m] = " << L << endl;
	cout << "Tout = " << T << endl;
	cout << "Cells = " << N << endl;
	cout << "Time[micro-seg] = " << T*dt*1e6 << endl;
	cout << "Frq[MHz] = " << fq*1e-6 << endl;
	cout << "Potency[kW] = " << potency*1e-3 << endl;
	cout << "***********" << endl;

	float eaf,ca,cb;  //auxiliary variables

	//static: It's global variable scope
	//during function calling "void abc()"
	static float l1,l2,r1,r2;

	//EM step-time loop
	for (int it=0; it<T; it++) {

		//update electric field
		for (int k=1; k<N; k++) {
			eaf = dt*sigma[k]/(2*EPS_r[k]*Const::EPS_0);
			ca = (1-eaf)/(1+eaf);
			cb = 0.5/(EPS_r[k]*(1+eaf));
			Ex[k] = ca*Ex[k] + cb*( Hy[k-1]-Hy[k] );
		}
		//add pulse source in middle position
		//Ex[N/2] += E0*sin(2*M_PI*fq*it*dt);
		Ex[1] += E0*sin(2*M_PI*fq*it*dt);

		//absorbin boundary condition
		abc(l1,l2,r1,r2,Ex);

		//update magnetic field
		for (int k=0; k<N-1; k++) {
			Hy[k] += 0.5*( Ex[k]-Ex[k+1] );
		}

	}//EM end step-time loop

	//ouput fields to CSV file
	outputFields((char*)"result.csv",0,dh,EPS_r,sigma*1e3,Ex);

}
